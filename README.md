
This project has been retired. I have since moved to [neovim](https://gitlab.com/ian-mitchell-001/vim-configs).

```
# Vim configs

The `.vimrc` I use for gvim. It's made to support

* C;
* Markdown;
* Python;
* Haskell;
* and LaTeX. 

Since it supports python, it should do well to serve most people. But the C, LaTeX, Markdown, and Haskell support should also be handy for anyone that uses those languages. 

## TODO

* ~~With my plans on learning haskell (the syntax is too attractive to bear), I'm planning on adding haskell syntax support and a macro for the haskell compiler.~~

* ~~Add some better syntax support for C and python.~~

* ~~Create LaTeX shortcuts.~~ (Not needed, since I use `pandoc` for most of my documents now.)
```

